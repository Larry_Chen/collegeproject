#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <limits.h>

static int bottom_up_cut_rod(int *p, int n);
static int *ex(int *p, int n);
static void print(int *p, int n);

int main(int argc, char *argv[])
{
	int row;
	int readfilearray[80];

	clock_t begin, finish;
	double duration;
	begin = clock();

	FILE *fptr;
	fptr = fopen("p2.txt", "r");
	for (row = 0; row < 80; row++)
	{
		fscanf(fptr, "%d", &readfilearray[row]);
	}

	fclose(fptr);
	if (fptr == NULL)
	{
		printf("FAILED");
	}
	fwrite(readfilearray, 1, sizeof(80), fptr);
	int p[40];
	int n = 40;
	int j = 0;
	int k = 0;
	for (int i = 1; i < 80; i = i + 2)
	{
		k++;
		printf("%d  %d\n", k, readfilearray[i]);

		p[j] = readfilearray[i];
		j = j + 1;
	}

	finish = clock();
	duration = ((double)(finish - begin)) / CLK_TCK;
	//print(p, n);

	printf("Rod length:%d\n", n);
	printf("MaxProfit:%d\n", bottom_up_cut_rod(p, n));

	printf("Cuts:");
	print(p, n);
	printf("\nBD所花時間:%fms\n", duration);

	while(0);

	return 0;
}

int bottom_up_cut_rod(int *p, int n)
{
	int r[100], q; //let r[0,n]be a new array
	r[0] = 0;
	for (int j = 1; j <= n; j++)
	{
		q = INT_MIN;
		for (int i = 1; i <= j; i++)
		{
			if (q >= p[i - 1] + r[j - i])
			{
				q = q;
			}
			else
			{
				q = p[i - 1] + r[j - i];
			}
		}
		r[j] = q;
	}

	return r[n];
}

int *ex(int *p, int n)
{
	int r[100], q;
	static int s[100];
	//r[0];
	for (int j = 1; j <= n; j++)
	{
		q = INT_MIN;
		for (int i = 1; i <= j; i++)
		{
			if (q < p[i - 1] + r[j - i])
			{
				q = p[i - 1] + r[j - i];
				s[j] = i;
			}
			r[j] = q;
		}
	}
	return s;
}

void print(int *p, int n)
{
	int r = bottom_up_cut_rod(p, n);
	//int s[100];
	int *pp = ex(p, n);
	while (n > 0)
	{
		printf("%d ", pp[n]);
		n = n - pp[n];
	}
}

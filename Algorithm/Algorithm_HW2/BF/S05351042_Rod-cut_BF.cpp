#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <limits.h>

static int cut_rod(int *p, int length);

int main(int argc, char *argv[])
{
	clock_t begin, finish;
	double duration;
	begin = clock();
	//int CLK_TCK = 1000;

	FILE *fptr;
	int readfilearray[16];
	fptr = fopen("p1.txt", "r");

	for (int row = 0; row < 16; row++)
		fscanf(fptr, "%d", &readfilearray[row]);

	fclose(fptr);

	if (fptr == NULL)
		printf("FAILED");

	fwrite(readfilearray, 1, sizeof(16), fptr);
	int p[8];
	int length = 8;
	int j = 0;
	int k = 0;
	for (int i = 1; i < 16; i = i + 2)
	{
		k++;
		printf("%d	%d\n", k, readfilearray[i]);

		p[j] = readfilearray[i];
		j++;
	}

	finish = clock();
	duration = ((double)(finish - begin)) / CLK_TCK;

	printf("Rod length:%d\n", length);
	printf("Max revenue:%d\n", cut_rod(p, length));
	printf("BF所花時間:%fms\n", duration);

	// system("pause");
	while (0)
		;

	return 0;
}

int cut_rod(int *p, int length)
{
	int q;
	if (length == 0)
	{
		return 0;
	}

	q = INT_MIN;
	for (int i = 1; i <= length; i++)
	{
		if (q > p[i - 1] + cut_rod(p, length - i))
		{
			q = q;
		}
		else
		{
			q = p[i - 1] + cut_rod(p, length - i);
		}
	}
	return q;
}

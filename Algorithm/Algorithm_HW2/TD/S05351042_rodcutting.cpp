#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <limits.h>

static int memorized_cut_rod(int *p, int length);
static int memorized_cut_rod_aux(int *p, int length, int *r);

int main(int argc, char *argv[])
{

	int row;
	int readfilearray[80];

	clock_t begin, finish;
	double duration;
	begin = clock();

	FILE *fptr;
	fptr = fopen("p2.txt", "r");
	for (row = 0; row < 80; row++)
	{
		fscanf(fptr, "%d", &readfilearray[row]);
	}

	fclose(fptr);
	if (fptr == NULL)
	{
		printf("FAILED");
	}
	fwrite(readfilearray, 1, sizeof(80), fptr);
	int p[40];
	int length = 40;
	int j = 0;
	int k = 0;
	for (int i = 1; i < 80; i = i + 2)
	{
		k++;
		printf("%d  %d\n", k, readfilearray[i]);

		p[j] = readfilearray[i];
		j++;
	}

	finish = clock();
	duration = ((double)(finish - begin)) / CLK_TCK;

	printf("Rod length:%d\n", length);
	printf("Max revenue:%d\n", memorized_cut_rod(p, length));
	printf("TD所花時間:%fms\n", duration);

	while (0);
	return 0;
}

int memorized_cut_rod(int *p, int length)
{
	int r[200];

	for (int i = 0; i <= length; i++)
	{
		r[i] = -500;
	}

	return memorized_cut_rod_aux(p, length, r);
}

int memorized_cut_rod_aux(int *p, int length, int *r)
{
	int q;

	if (r[length] >= 0)
	{
		return r[length];
	}

	if (length == 0)
	{
		q = 0;
	}
	else
	{
		q = -1000;
	}

	for (int i = 1; i <= length; i++)
	{
		if (q > p[i - 1] + memorized_cut_rod_aux(p, length - i, r))
		{
			q = q;
		}
		else if (q <= p[i - 1] + memorized_cut_rod_aux(p, length - i, r))
		{
			q = p[i - 1] + memorized_cut_rod_aux(p, length - i, r);
		}
		r[length] = q;
	}
	return q;
}

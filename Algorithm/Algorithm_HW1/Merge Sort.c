#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <string.h> //讀檔函式庫
#include <time.h>   //計算時間函式庫

using namespace std;
static float crossing_array(float *input, int low, int mid, int high); //定義函式
static float max_subarray(float *input, int low, int high);            //定義函式

int _tmain(int argc, _TCHAR *argv[])
{
    clock_t time1, time2; //宣告clock時間
    time1 = clock();      //由於 clock 回傳的數值是以毫秒為單位，所以會在除上一個 CLOCKS_PER_SEC，這個 CLOCKS_PER_SEC 通常都是 1000

    FILE *fPtr;
    float input[17];
    fPtr = fopen("input.txt", "r"); //使用stdlio.h的fopen()函數開檔，FILE * fopen (filename,mode);
    int row;
    fscanf_s(fPtr, "%f", &row);
    for (int a = 0; a < 17; a++)         //讀18個字元
        fscanf_s(fPtr, "%f", &input[a]); //讀入字元
    fclose(fPtr);                        //關檔
    if (fPtr == NULL)
    {
        printf("開讀檔失敗!");
    } //如果讀檔失敗就印開讀檔失敗//float A[7] = {1.5,15,20.5,-3,-13,7,5};

    fwrite(input, 1, sizeof(17), fPtr); //寫檔size_t fwrite (array或是struct的指標,每一個元素被寫入的大小,寫入元素數量,指向FILE檔案指標);

    float sub = 0;               //找價差
    float price[16];             //把價差存成陣列
    for (int i = 0; i < 15; i++) //算出價差並存入price[i]
    {
        sub = input[i + 1] - input[i];
        printf("%.2f\n", sub);
        price[i] = sub;
    }

    //float fl=max_subarray(A,0,6);
    printf("獲利為:%.2f\n", max_subarray(price, 0, 17)); //印出結果
    //printf("%f",crossing_array(int max_left,int max_right));

    time2 = clock();
    printf("執行時間:%f\n", (time2 - time1) / (double)(CLOCKS_PER_SEC));
    system("pause");
    return 0;
}
float crossing_array(float *input, int low, int mid, int high)
{
    float left_sum = 0, right_sum = 0; //左邊和右邊總和設無限小
    float sum = 0;                     //設初值
    //float high,mid,low;
    int max_left, max_right;
    //find a maximun subarray of the form A[i...mid]
    for (int i = mid; i >= 0; i--) //從mid的位置往下找，找到最小位置
    {

        sum = sum + input[i]; //當sum > ledt_sum時，把sum =  sum + input[i]的值存入left_sum

        if (sum > left_sum)
        {
            left_sum = sum;
            max_left = i;
        }
    }
    sum = 0;                            //歸零sum，使得sum可以給下一個迴圈使用
                                        //find a maximum subarray of the form A[mid + 1..j]
    for (int j = mid + 1; j <= 16; j++) //從mid+1的位置往上找，找到最大位置
    {
        //sum = 0;
        sum = sum + input[j];
        if (sum > right_sum) //當sum > right_sum時，把sum =  sum + input[j]的值存入right_sum
        {
            right_sum = sum;
            max_right = j;
        }
    }
    //	printf("%d  %d",max_left,max_right);
    return (left_sum + right_sum); //
}

float max_subarray(float *input, int low, int high)
{
    int mid;                                                                                                                         //int
    float left_low = 0, left_high = 0, left_sum, right_low = 0, right_high = 0, right_sum, cross_low = 0, cross_high = 0, cross_sum; //define a lot of things....
    if (high == low)
    {
        return (input[low]); //only call one value
    }

    else
    {
        mid = (low + high) / 2; //need to get floor

        left_sum = max_subarray(input, low, mid);

        right_sum = max_subarray(input, mid + 1, high);

        cross_sum = crossing_array(input, low, mid, high);
        //printf("L %f\n\n",left_sum);
        //printf("R%f\n\n",right_sum);
    }

    if (left_sum >= right_sum && left_sum >= cross_sum)
    {
        //printf("l %f\n\n",left_sum);
        return (left_sum);
    }
    else if (right_sum >= left_sum && right_sum >= cross_sum)
    {
        //printf("r %f\n\n",right_sum);
        return (right_sum);
    }
    else
    {
        //printf("%f\n\n",cross_sum);
        return (cross_sum);
    }
}

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>   //計算時間函式庫
#include <string.h> //讀檔函式庫

int _tmain(int argc, _TCHAR *argv[])
{
    FILE *fPtr;
    int input[17];
    fPtr = fopen("input.txt", "r"); //使用stdlio.h的fopen()函數開檔，FILE * fopen (filename,mode);
    int row;
    fscanf_s(fPtr, "%f", &row);
    for (int a = 0; a < 18; a++)         //讀18個字元
        fscanf_s(fPtr, "%d", &input[a]); //讀入字元
    fclose(fPtr);                        //關檔
    if (fPtr == NULL)
    {
        printf("開讀檔失敗!");
    } //如果讀檔失敗就印開讀檔失敗

    fwrite(input, 1, sizeof(17), fPtr); //寫檔size_t fwrite (array或是struct的指標,每一個元素被寫入的大小,寫入元素數量,指向FILE檔案指標);

    clock_t time1, time2; //宣告clock時間
    time1 = clock();      //由於 clock 回傳的數值是以毫秒為單位，所以會在除上一個 CLOCKS_PER_SEC，這個 CLOCKS_PER_SEC 通常都是 1000
                          //int n ;共有幾筆資料		//測試數據
                          //int v[17] = {100,113,110,85,105,102,86,63,81,101,94,106,101,79,94,90,97};		//測試數據
                          //scanf_s("%d",&n);		//測試數據
                          //printf("共%d筆資料\n",n);		//測試數據
    int low = input[0];   //設最低點
    int profit = 0;
    int sale; //賣出
    int buy;  //買入
    int a;    //預設找價差

    for (int i = 0; i < 17; i++)
    {
        if (low > input[i]) //如果low值大於v[i]   low=找最低點
        {
            low = input[i]; //把low值存進v[i]值
            buy = i;        //第幾天買入
        }
        a = input[i] - low; //真實價差
        if (a > profit)     //如果價差大於最高利潤
        {
            profit = a; //把利潤最高值存進profit
            sale = i;   //第幾天賣出
        }
    }
    printf("%d 第%d天買入 第%d天賣出\n", profit, buy, sale); //最高利潤,何時買入,何時賣出
    time2 = clock();
    printf("執行時間:%f\n", (time2 - time1) / (double)(CLOCKS_PER_SEC));
    system("pause");
}

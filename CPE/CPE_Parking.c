#include <stdio.h>
#include <stdlib.h>

int main() {
    
    int casenum;
    scanf("%d", &casenum);

    for (int i = 0; i < casenum; i++) {
        int numstores, min = 100, max = -1;
        scanf("%d", &numstores);
        
        for (int j = 0; j < numstores; j++) {
            int p;
            scanf("%d", &p);

            if (p > max) {
                max = p;
            }

            if (p < min) {
                min = p;
            }
        }

        printf("%d\n", (max-min)*2);        
    }
    return 0;
}
#include <stdio.h>
#include <stdlib.h>

int main(){

    int input, n, count; 
    printf("請輸入完全平方數:");
    scanf("%d", &n);
    
    for(int i = 1; i < n; i++){
        if((i*i) <= n){
            printf("%d\n", i*i);
            count = i;            
        }
    }
    printf("%d個\n", count);
    
    return 0;
}
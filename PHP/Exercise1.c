#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    
    // KNOWLEDGE = 96
    // HARDWORK = 98
    // ATTITUDE = 100

    int letter_num[26], sum = 0, temp = 0;
    char input[20];
    
    
    scanf("%s", input);
    for(int i = 0; i < strlen(input); i++){
        if(input[i] > 96){
            temp = temp + input[i]-96;
        }
        else if(input[i] < 65 || input[i] > 122){
            temp = 0;
        }
        else
        temp = temp + input[i]-64;
        
    } 
    if(temp != 0){
        printf("%s's Value: %d\n", input, temp);
    }
    else{
        printf("FAIL\n");
    }
    
    

    return 0;
}
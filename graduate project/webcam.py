from mvnc import mvncapi as ncs
import numpy as np
import cv2
import time

# GRAPH = 'project'
# IMAGE = 'images/cat.jpg'
CLASSES =  ('background',
           'aeroplane', 'bicycle', 'bird', 'boat',
           'bottle', 'bus', 'car', 'cat', 'chair',
           'cow', 'diningtable', 'dog', 'horse',
           'motorbike', 'person', 'pottedplant',
           'sheep', 'sofa', 'train', 'tvmonitor')

input_size = (300, 300)
np.random.seed(3)
color = 255 * np.random.rand(len(CLASSES), 3)

# discover our device, initialize and open a device
device_list = ncs.enumerate_devices()
if len(device_list) == 0:
    print('No devices found')
    quit()

device = ncs.Device(device_list[0])
device.open()


# Load graph onto the device; initialize a graph from file at some graph_filepath
GRAPH_FILEPATH = './graph' # need to modify to self Graph_Path
with open(GRAPH_FILEPATH, mode='rb') as f:
	graph_buffer = f.read()
    
graph = ncs.Graph('graph1')
input_fifo, output_fifo = graph.allocate_with_fifos(device, graph_buffer)

# image preprocessing; like input_tensor
# Load preprocessing data; mean = 128; std = 1/128
def preprocess(src):
    img = cv2.resize(src, input_size)
    img = img - 127.5
    img = img / 127.5
    return img.astype(np.float16)

# graph => load the image to it, return a prediction
capture = cv2.VideoCapture(0)
_, image = capture.read()
height, width = image.shape[:2]

# ***************************************************************
# input_fifo_type & FP16
# ***************************************************************
# input_fifo_type = ncs.FifoType.Host_WO
# input_fifo_data_type = ncs.FifoDataType.FP16
# input_fifo_num_elem = 2

# output_fifo_type = ncs.FifoType.Host_RO
# output_fifo_data_type = ncs.FifoDataType.FP16
# output_fifo_num_elem = 2
#input_fifo, output_fifo = graph.allocate_with_fifos(device, graph_buffer,
                                                    # input_fifo_type, input_fifo_data_type, input_fifo_num_elem, 
                                                    # output_fifo_type, output_fifo_data_type, output_fifo_num_elem)
# input_fifo, output_fifo = graph.allocate_with_fifos(device, graph_buffer)
# ***************************************************************

while True:
    stime = time.time()
    _, image = capture.read()
    image_pro = preprocess(image)
    graph.queue_inference_with_fifo_elem(input_fifo, output_fifo, image_pro, None) # 'object_user'
    output, user_obj = output_fifo.read_elem()

    valid_boxes = int(output[0])

    for i in range(7, 7 * (1 + valid_boxes), 7):
        if not np.isfinite(sum(output[i +1: i + 7])):
            continue
        clss = CLASSES[int(output[i+1])]
        confidence = output[i+2]
        color = colors[int(output[i+1])]
        
        x1 = max(0, int(output[i+3] * width))
        y1 = max(0, int(output[i+4] * height))
        x2 = min(width, int(output[i+5] * width))
        y2 = min(height, int(output[i+6] * height))
    
        label = '{}: {:.0f}'.format(clss, confidence * 100)
        image = cv2.rectangle(image, (x1, y1), (x2, y2), color, 2)
        y = y1 - 5 if y1 - 15 > 15 else y1 + 18 #let label do not out of the frame
        image = cv2.putText(image, label, (x1-5, y), cv2.FONT_HERSHEY_SIMPLEX, 0.8, color, 2)
    
    cv2.imshow('frame', image)
    print('FPS = {:.1f}'.format(1 / (time.time() - stime)))
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Clean up
capture.release()
cv2.destroyAllWindows()
input_fifo.destroy()
output_fifo.destroy()
graph.destroy()
device.close()
device.destroy()

# capture.release()
# cv2.destroyAllWindows()
# device.CloseDevice()

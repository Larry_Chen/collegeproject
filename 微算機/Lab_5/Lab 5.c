#define GPIO_ACT_PE *((unsigned int *)0x001F6A00)  //定義
#define GPIO_OEN_PE *((unsigned int *)0x001F6A04)  //定義
#define GPIO_OMOD_PE *((unsigned int *)0x001F6A08) //定義
#define GPIO_DAT_PE *((unsigned int *)0x001F6A10)  //定義
#define GPIO_ACT_PB *((unsigned int *)0x001F6880)  //定義
#define GPIO_OEN_PB *((unsigned int *)0x001F6884)  //定義
#define GPIO_OMOD_PB *((unsigned int *)0x001F6888) //定義
#define PAD_PB *((unsigned int *)0x001F688c)       //定義
#define GPIO_DAT_PB *((unsigned int *)0x001F6890)  //定義
#define GPIO_REN_PB *((unsigned int *)0x001F6894)  //定義
#define GPIO_RS_PB *((unsigned int *)0x001F6898)   //定義

int seg[16] = {0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x27, 0x7f, 0x67, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71};
long position[4] = {0x0000, 0x2000, 0x4000, 0x6000};

#define GPIO_ACT_PD *((unsigned int *)0x001F6980)  //定義
#define GPIO_OEN_PD *((unsigned int *)0x001F6984)  //定義
#define GPIO_OMOD_PD *((unsigned int *)0x001F6988) //定義
#define PAD_PD *((unsigned int *)0x001F698C)       //定義
#define GPIO_DAT_PD *((unsigned int *)0x001F6990)  //定義
#define GPIO_REN_PD *((unsigned int *)0x001F6994)  //定義
#define GPIO_RS_PD *((unsigned int *)0x001F6998)   //定義
#define GPIO_BR_PD *((unsigned int *)0x001F699C)   //定義
#define GPIO_BS_PD *((unsigned int *)0x001F69A0)   //定義

#define RS 0x0080       //定義
#define E 0x0040        //定義
#define RW 0x0020       //定義
#define CleanSet 0x0000 //定義

#define System_Control_18 *((unsigned int *)0x00200018) //定義
#define System_Control_20 *((unsigned int *)0x00200020) //定義
#define RTC_CTR *((unsigned int *)0x00201038)           //定義

#define RTC_SEC *((unsigned int *)0x00201000)   //定義
#define RTC_MIN *((unsigned int *)0x00201004)   //定義
#define RTC_HOUR *((unsigned int *)0x00201008)  //定義
#define RTC_DAY *((unsigned int *)0x0020100c)   //定義
#define RTC_WEEK *((unsigned int *)0x00201010)  //定義
#define RTC_MONTH *((unsigned int *)0x00201014) //定義
#define RTC_YEAR *((unsigned int *)0x00201018)  //定義

void delay1(unsigned int nCount)
{
    unsigned int i;
    for (i = 0; i < nCount; i++)
        ;
}

void CheckBusy(void) //BF ,CHACK BUSY FLAG
{
    unsigned short int i = 0x8000;
    while (i & 0x8000)
    {
        GPIO_ACT_PD = 0xFFFF; //Initialize GPIO_D outpot
        GPIO_OMOD_PD = 0x0;
        GPIO_OEN_PD = 0x0;

        GPIO_DAT_PD = (RW + E);

        GPIO_ACT_PD = 0xFF00; //Initialize GPIO_D input
        GPIO_RS_PD = 0xFF00;
        GPIO_REN_PD = 0xFF00;

        i = PAD_PD;

        GPIO_DAT_PD = CleanSet;
        delay1(100000);
    }
}

void WriteData(unsigned short int i)
{
    GPIO_ACT_PD = 0xFFFF; //Initialize GPIO_D outpot
    GPIO_OEN_PD = 0x001F;
    GPIO_OMOD_PD = 0xFFE0;
    GPIO_BR_PD = E;
    GPIO_BS_PD = RS;
    GPIO_BR_PD = RW;
    //GPIO_OMOD_PD = 0x0;
    //GPIO_OEN_PD = 0x0;

    GPIO_DAT_PD = ((i << 8) + RS + E);
    delay1(10);
    GPIO_BS_PD = E;

    delay1(100);
    GPIO_BR_PD = E;
}

void WriteIns(unsigned short int instruction)
{
    GPIO_ACT_PD = 0xFFFF; //Initialize GPIO_D outpot
    GPIO_OMOD_PD = 0x0;
    GPIO_OEN_PD = 0x0;

    GPIO_DAT_PD = (instruction + E); // 指令?入操作：?入 RS=低、RW=低、EP=上升沿； ?出：?；
    GPIO_DAT_PD = CleanSet;

    unsigned short int i = 0x8000;
    while (i & 0x8000)
    {
        GPIO_ACT_PD = 0xFFFF; //Initialize GPIO_D outpot
        GPIO_OMOD_PD = 0x0;
        GPIO_OEN_PD = 0x0;

        GPIO_DAT_PD = (RW + E);

        GPIO_ACT_PD = 0xFF00; //Initialize GPIO_D input
        GPIO_RS_PD = 0xFF00;
        GPIO_REN_PD = 0xFF00;

        i = PAD_PD;

        GPIO_DAT_PD = CleanSet;
        delay1(100000);
    }
}

void InitialLCD(void)
{
    WriteIns(0x3800); //FUNCTION SET
    WriteIns(0x3800);
    WriteIns(0x3800);
    WriteIns(0x3800);
    WriteIns(0x0800); // off display
    WriteIns(0x0100); // clear buffer
    WriteIns(0x0e00); // on display
    WriteIns(0x0600); // set input mode
}
void LCD_CLEAR()
{
    WriteIns(0x0100);
}
void LCD_NUMBER(unsigned int number)
{
    //WriteIns(0x3800);  //FUNCTION SET
    //WriteIns(0x0C00);  //DISPLAY CONTROL
    //WriteIns(0x0600);  //SET INPUT MODE
    WriteData(number);
    //WriteIns(0x0600);
}

void LCD_STR(char *str)
{
    //char i=0;
    WriteIns(0x3800); //FUNCTION SET
    WriteIns(0x0C00); //DISPLAY CONTROL
    WriteIns(0x0600); //SET INPUT MODE
    while (*str != 0)
        WriteData(*str++);
    //WriteIns(0x0600);
}
void SET_POS(unsigned char x, unsigned char y)
{
    unsigned int cmd;
    if (x < 1)
        x = 1;
    if (y < 1)
        y = 1;
    cmd = (x - 1) * 0x40 + (y - 1) + 0x80;
    WriteIns(0x3800); //FUNCTION SET
    WriteIns((cmd << 8));
    //WriteIns(0x0600);  //SET INPUT MODE
}
int myatoi(const char *str)
{
    int result = 0;
    while (*str != '\0')
    {
        result *= 10;
        //把字元-'0' 就會變成數字 和 -48同意義
        result += *str - '0';
        str++;
    }
    return result;
}
int main()
{
    unsigned int sec, min, hour;
    char result_str[16] = "";
    System_Control_18 = (System_Control_18 | 0x0060);
    System_Control_20 = (System_Control_20 | 0x0005);
    RTC_CTR = 0x07;
    RTC_MONTH = 12; //只有month是10進位
    RTC_DAY = 0x05; //其餘都是16進位
    RTC_HOUR = 0x23;
    RTC_MIN = 0x59;
    RTC_SEC = 0x50;

    InitialLCD(); //初始化LCD

    GPIO_ACT_PB = 0xFFFF; //Initialize GPIO
    GPIO_OMOD_PB = 0x0;
    GPIO_OEN_PB = 0x0;
    GPIO_ACT_PE = 0xFFFF; //Initialize GPIO
    GPIO_OMOD_PE = 0x0;
    GPIO_OEN_PE = 0x0;

    InitialLCD(); //初始化LCD

    while (1)
    {
        min = RTC_MIN;
        hour = RTC_HOUR;
        sec = RTC_SEC;

        SET_POS(1, 1);
        LCD_STR("2018-");
        LCD_NUMBER((RTC_MONTH / 10) + 0x30); //因為有兩個數字
        LCD_NUMBER((RTC_MONTH % 10) + 0x30); //把month取10餘數 +0x30為了讓他顯示出來為數字

        LCD_STR("-");

        LCD_NUMBER((RTC_DAY) + 0x30);

        SET_POS(2, 1);

        LCD_NUMBER((hour / 16) + 0x30); //因為有兩個數字
        LCD_NUMBER((hour % 16) + 0x30); //把hour取16餘數 +0x30為了讓他顯示出來為數字
        LCD_STR(":");

        LCD_NUMBER((min / 16) + 0x30); //因為有兩個數字
        LCD_NUMBER((min % 16) + 0x30); //把min取16餘數 +0x30為了讓他顯示出來為數字
        LCD_STR(":");

        LCD_NUMBER((sec / 16) + 0x30); //因為有兩個數字
        LCD_NUMBER((sec % 16) + 0x30); //把sec取16餘數 +0x30為了讓他顯示出來為數字

        //delay1(100000);
    }

    return 0;
}
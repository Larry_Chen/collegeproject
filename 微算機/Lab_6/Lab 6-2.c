#define System_Control_00 *((unsigned int *)0x00200000)
#define System_Control_04 *((unsigned int *)0x00200004)
#define TRMO_TCTL *((unsigned int *)0x00201c00)
#define TRMO_PSCL *((unsigned int *)0x00201c08)
#define TRMO_MATA0 *((unsigned int *)0x00201c30)
#define TRMO_IE *((unsigned int *)0x00201c44)
#define TRMO_TMSTA *((unsigned int *)0x00201c40)
#include "HARDWARE.h"

#define To_ASCII 0x30

extern void LCMInit();
extern unsigned int LCMCheckBusyAdr();
extern void LCMCMDWR();
extern void LCMDATAWR();
extern void LCMSTR();
extern void LCMSetPos();
extern unsigned int LCMDATARD();

int seg[16] = {0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x27, 0x7f, 0x67, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71};
long position[8] = {0x6000, 0x4000, 0x2000, 0x0000, 0xE000, 0xC000, 0xA000, 0xB000};
void delay1(unsigned int nCount)
{
    unsigned int i;
    for (i = 0; i < nCount; i++)
        ;
}

void InitialTMR0(void)
{
    TMR0_TCTL &= (~(0x1 << TCTL_EN_OffSet)) & (~(0x1 << TCTL_START_OffSet));
    TMR0_TCTL |= (1 << TCTL_RST_OffSet);
    TMR0_PSCL = 23999; //prescaler, counting base clock =1KHz
    TMR0_TCTL |= 1 << TCTL_EN_OffSet;
    TMR0_TCTL &= ~(0x1 << TCTL_RST_OffSet);
}

int main()
{
    System_Control_00 = (System_Control_00 | 0x000E); //使用外部石英震盪器
    System_Control_04 = (System_Control_04 | 0x0001); // Switch clock source from external crystal.
    InitialTMR0();
    *((unsigned int *)(TIMER0 + MAT0A_OFF)) = 1000; //1 sec
    *((unsigned int *)(TIMER0 + TCIE_OffSet)) |= 1 << MAT0AIE_OffSet;

    TMR0_TCTL |= 1 << TCTL_START_OffSet;

    GPIO_ACT_PB = 0xFFFF; //Initialize GPIO
    GPIO_OMOD_PB = 0x0;
    GPIO_OEN_PB = 0x0;

    GPIO_ACT_PE = 0xFFFF; //Initialize GPIO
    GPIO_OMOD_PE = 0x0;
    GPIO_OEN_PE = 0x0;
    int a = 0, b = 0, c = 0, d = 0, e = 0;
    while (1)
    {

        if (TRMO_TMSTA && (1 << 4) != 0) //if_mata0a interrupt
        {
            a = a + 1;              //a = count
            b = a % 10;             //個位數
            c = (a / 10) % 10;      //十位數
            d = (a / 100) % 10;     //百位數
            e = (a / 1000) % 10;    //千位數
            TMR0_TCTL |= (1 << 1);  // TMR0_TCTL  reset
            TMR0_TCTL &= ~(1 << 1); // TMR0_TCTL  reset cancel
            TRMO_TMSTA |= (1 << 4); // if_mata0a reset
        }
        GPIO_DAT_PB = position[0]; //設定SEG第一個
        GPIO_DAT_PE = seg[b];
        delay1(10000);

        GPIO_DAT_PB = position[1]; //設定SEG第二個
        GPIO_DAT_PE = seg[c];
        delay1(10000);

        GPIO_DAT_PB = position[2]; //設定SEG第三個
        GPIO_DAT_PE = seg[d];
        delay1(10000);

        GPIO_DAT_PB = position[3]; //設定SEG第四個
        GPIO_DAT_PE = seg[e];
        delay1(10000);
    }
    return 0;
}
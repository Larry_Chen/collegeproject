#define System_Control_00 *((unsigned int *)0x00200000)
#define System_Control_04 *((unsigned int *)0x00200004)
#define TRMO_TCTL *((unsigned int *)0x00201c00)
#define TRMO_PSCL *((unsigned int *)0x00201c08)
#define TRMO_MATA0 *((unsigned int *)0x00201c30)
#define TRMO_IE *((unsigned int *)0x00201c44)
#define TRMO_TMSTA *((unsigned int *)0x00201c40)
#include <stdio.h>
#include "HARDWARE.h"
#define To_ASCII 0x30
extern void LCMInit();
extern unsigned int LCMCheckBusyAdr();
extern void LCMCMDWR();
extern void LCMDATAWR();
extern void LCMSTR();
extern void LCMSetPos();
extern unsigned int LCMDATARD();
void delay1(unsigned int nCount)
{
    unsigned int i;
    for (i = 0; i < nCount; i++)
        ;
}

void InitialTMR0(void)
{
    //TMR0_TCTL &= (~(0x1 << TCTL_EN_OffSet)) & (~(0x1 << TCTL_START_OffSet )) ;
    TMR0_TCTL |= (1 << TCTL_RST_OffSet); //counter reset
    TMR0_PSCL = 23999;                   //prescaler, counting base clock =1KHz 除貧
    TMR0_TCTL |= 1 << TCTL_EN_OffSet;    //計時器enable
    TMR0_TCTL &= ~(0x1 << TCTL_RST_OffSet);
}

int main()
{
    /*TMR0_TCTL |= ( 1<< 1);  // TMR0_TCTL [1] reset
	TMR0_PSCL = 23999;
	TMR0_TCTL |= 1<< 15; //  TMR0_TCTL[15]  enable
	TMR0_TCTL &= ~( 1<< 1); // TMR0_TCTL [1] reset close
	TRMO_MATA0 = 1000;
	TRMO_IE |= 1 <<4 // TMR0_IE[4]  ie_mat0a enable
	TRMO_TCTL |= 1<< 0; //  TMR0_TCTL[0]  start
	*/
    int a = 0;
    System_Control_00 = (System_Control_00 | 0x000E); //使用外部石英震盪器
    System_Control_04 = (System_Control_04 | 0x0001); // Switch clock source from external crystal.
    InitialTMR0();
    *((unsigned int *)(TIMER0 + MAT0A_OFF)) = 1000;                   //1 sec 中斷器
    *((unsigned int *)(TIMER0 + TCIE_OffSet)) |= 1 << MAT0AIE_OffSet; //讀取中斷器
    TMR0_TCTL |= 1 << TCTL_START_OffSet;                              //開始
    LCMInit();

    while (LCMCheckBusyAdr() & 0x80)
        ;
    LCMSetPos(1, 1);
    while (LCMCheckBusyAdr() & 0x80)
        ;
    LCMSTR("counter=");

    while (1)
    {
        if (TRMO_TMSTA && (1 << 4) != 0)
        {
            a = a + 1; //count

            TMR0_TCTL |= (1 << 1);
            TMR0_TCTL &= ~(1 << 1);
            TRMO_TMSTA |= (1 << 4);
        }
        char team[200];         //設定字串
        sprintf(team, "%d", a); //把a的值轉進去字串team裡
        LCMSetPos(2, 1);
        LCMSTR(team);
    }
    return 0;
}
#define GPIO_ACT_PE	*((unsigned int *)0x001F6A00)   //定義位址
#define GPIO_OEN_PE 	*((unsigned int *)0x001F6A04)   //定義位址
#define GPIO_OMOD_PE	*((unsigned int *)0x001F6A08)   //定義位址
#define GPIO_DAT_PE	*((unsigned int *)0x001F6A10)   //定義位址

#define GPIO_ACT_PB	*((unsigned int *)0x001F6880)   //定義位址
#define GPIO_OEN_PB 	*((unsigned int *)0x001F6884)   //定義位址
#define GPIO_OMOD_PB	*((unsigned int *)0x001F6888)   //定義位址
#define PAD_PB	 	*((unsigned int *)0x001F688c)   //定義位址
#define GPIO_DAT_PB	*((unsigned int *)0x001F6890)   //定義位址

#define GPIO_ACT_PA	*((unsigned int *)0x001F6800)   //定義位址
#define GPIO_OEN_PA 	*((unsigned int *)0x001F6804)   //定義位址
#define GPIO_OMOD_PA	*((unsigned int *)0x001F6808)   //定義位址
#define PAD_PA	 	*((unsigned int *)0x001F680c)   //定義位址
#define GPIO_DAT_PA	*((unsigned int *)0x001F6810)   //定義位址
#define GPIO_REN_PA 	*((unsigned int *)0x001F6814)   //定義位址
#define GPIO_RS_PA 	*((unsigned int *)0x001F6818)   //定義位址

int seg[16]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x27,0x7f,0x67,0x77,0x7c,0x39,0x5e,0x79,0x71};  //宣告能使7SEG顯示0~F的pin DP~A值
long position[8]={0x6000,0x4000,0x2000,0x0000,0xE000,0xC000,0xA000,0xB000}; //宣告7SEG的對應位址

void delay1(unsigned int nCount)
{
   unsigned int i;
   for(i=0;i<nCount;i++);
}


int main()
{
			GPIO_ACT_PB = 0xFFFF;   //設定初始值
			GPIO_OMOD_PB = 0x0;     //設定初始值
			GPIO_OEN_PB = 0x0;      //設定初始值

			GPIO_ACT_PA = 0x01FE;   //設定初始值
			GPIO_RS_PA = 0x01FE;    //設定初始值
			GPIO_REN_PA = 0x01FE;   //設定初始值

			GPIO_ACT_PE =0x00FF;    //設定初始值
			GPIO_OMOD_PE =0xFF00;   //設定初始值
			GPIO_OEN_PE =0xFF00;    //設定初始值
			int x,a,b;

			while(1){
				x = PAD_PA;     //讀取案件直放到x
				x = x/2;        //資料右移1
				a = x % 16;     //以除以16的餘數，取8bit資料的後4bit
				b = x / 16;     //資料右移4
				b = b % 16;     //以除以16的餘數，取8bit資料的前4bit

					GPIO_DAT_PB = position[0];      //指定7 SEG_1的位址
					GPIO_DAT_PE = seg[a];           //指定7 SEG_1的資料
					delay1(1000000*0.01);           //call delay

					GPIO_DAT_PB = position[1];      //指定7 SEG_2的位址
					GPIO_DAT_PE = seg[b];           //指定7 SEG_2的資料
					delay1(1000000*0.01);           //call delay
			}
	return 0;
}

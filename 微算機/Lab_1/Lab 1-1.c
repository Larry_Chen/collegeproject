#define GPIO_ACT_PE	*((unsigned int *)0x001F6A00)	//定義位址
#define GPIO_OEN_PE 	*((unsigned int *)0x001F6A04) 	//定義位址
#define GPIO_OMOD_PE	*((unsigned int *)0x001F6A08)	//定義位址
#define GPIO_DAT_PE	*((unsigned int *)0x001F6A10)	//定義位址

#define GPIO_ACT_PB	*((unsigned int *)0x001F6880)	//定義位址
#define GPIO_OEN_PB 	*((unsigned int *)0x001F6884)	//定義位址
#define GPIO_OMOD_PB	*((unsigned int *)0x001F6888)	//定義位址
#define PAD_PB	 	*((unsigned int *)0x001F688C)	//定義位址
#define GPIO_DAT_PB	*((unsigned int *)0x001F6890)	//定義位址


int seg[16]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x27,0x7f,0x67,0x77,0x7c,0x39,0x5e,0x79,0x71};	//宣告能使7SEG顯示0~F的pin DP~A值
long position[8]={0x6000,0x4000,0x2000,0x0000,0xE000,0xC000,0xA000,0xB000};
			//宣告7SEG的對應位址
void delay1(unsigned int nCount)
{
   unsigned int i;
   for(i=0;i<nCount;i++);
}


int main()
{

	GPIO_ACT_PB = 0xFFFF; //Initialize GPIO
	GPIO_OMOD_PB = 0x0; //設定初始值
	GPIO_OEN_PB = 0x0;	//設定初始值

	GPIO_ACT_PE = 0xFFFF; //Initialize GPIO
	GPIO_OMOD_PE = 0x0;	//設定初始值
	GPIO_OEN_PE = 0x0;	//設定初始值

	int digit =0;
	int count=0;
	int I,j,k ;
	

	while(1){
		for(i = 0;i<=9;i++)
			{
			for(j = 0;j<=9;j++)
				{
				for(k = 0;k<100;k++)			//跑100次
					{
					GPIO_DAT_PB = position[0];	//第0個7段顯示器
					GPIO_DAT_PE = seg[j];		//從0-9
					delay1(1000000*0.01);		//延遲

					GPIO_DAT_PB = position[1];	//第1個7段顯示器
					GPIO_DAT_PE = seg[i];		//從0-9
					delay1(1000000*0.01);		//延遲
					}
				}
				delay1(0.001);					//延遲
}

		}
	return 0;
}

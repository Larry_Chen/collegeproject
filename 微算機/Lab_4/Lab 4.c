#define GPIO_ACT_PE *((unsigned int *)0x001F6A00)  //定義
#define GPIO_OEN_PE *((unsigned int *)0x001F6A04)  //定義
#define GPIO_OMOD_PE *((unsigned int *)0x001F6A08) //定義
#define GPIO_DAT_PE *((unsigned int *)0x001F6A10)  //定義

#define GPIO_ACT_PC *((unsigned int *)0x001F6900)  //定義
#define GPIO_OEN_PC *((unsigned int *)0x001F6904)  //定義
#define GPIO_OMOD_PC *((unsigned int *)0x001F6908) //定義
#define PAD_PC *((unsigned int *)0x001F690c)       //定義
#define GPIO_DAT_PC *((unsigned int *)0x001F6910)  //定義
#define GPIO_REN_PC *((unsigned int *)0x001F6914)  //定義
#define GPIO_RS_PC *((unsigned int *)0x001F6918)   //定義

#define GPIO_ACT_PB *((unsigned int *)0x001F6880)  //定義
#define GPIO_OEN_PB *((unsigned int *)0x001F6884)  //定義
#define GPIO_OMOD_PB *((unsigned int *)0x001F6888) //定義
#define PAD_PB *((unsigned int *)0x001F688c)       //定義
#define GPIO_DAT_PB *((unsigned int *)0x001F6890)  //定義
#define GPIO_REN_PB *((unsigned int *)0x001F6894)  //定義
#define GPIO_RS_PB *((unsigned int *)0x001F6898)   //定義
int seg[16] = {0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x27, 0x7f, 0x67, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71};
long position[4] = {0x0000, 0x2000, 0x4000, 0x6000};
unsigned int scanline = 8;

void delay1(unsigned int nCount) //定義delay
{
    unsigned int i;
    for (i = 0; i < nCount; i++)
        ;
}

int main()
{
    //4*4 Key_PAD and 7-SEG
    GPIO_ACT_PC = 0x00FF; //0~7 In-Out mode
    GPIO_OEN_PC = 0x00F0; //0~3 output
    GPIO_RS_PC = 0x0FFF;
    GPIO_REN_PC = 0x0FFF;

    GPIO_ACT_PE = 0xFFFF;
    GPIO_OMOD_PE = 0xFF00;
    GPIO_OEN_PE = 0xFF00;
    GPIO_DAT_PE = 0x0000;

    GPIO_ACT_PB = 0xFFFF; //Initialize GPIO
    GPIO_OMOD_PB = 0x0;
    GPIO_OEN_PB = 0x0;

    int col = 0, row = 0; //定義變數col row =0
    int a = 0;            //定義a=0
    while (1)
    {

        for (col = 0; col < 4; col++) //col0~ccol3逐一判斷
        {
            GPIO_DAT_PC = ~scanline; //輸出掃描信號
            while ((PAD_PC & 0xF0) != 0xF0)
            {
                delay1(100000);
                if ((PAD_PC & 0xF0) != 0xF0) //把padpc讀進來跟F0做and
                {
                    if ((PAD_PC & 0xF0) == 0xE0) //若等於E0
                    {
                        row = 0;
                        a = col;
                    }
                    else if ((PAD_PC & 0xF0) == 0xD0) //若等於D0
                    {
                        row = 1;
                        a = col;
                    }
                    else if ((PAD_PC & 0xF0) == 0xB0) //若等於B0
                    {
                        row = 2;
                        a = col;
                    }
                    else if ((PAD_PC & 0xF0) == 0x70) //若等於70
                    {
                        row = 3;
                        a = col;
                    }
                    //while((PAD_PC&0xF0)!=0xF0){}
                }
            }
            if (scanline == 1) //掃完之後
                scanline = 8;  //回歸到8
            else
                scanline = scanline >> 1; //掃描循環
        }
        GPIO_DAT_PB = position[1];      //印出位置
        GPIO_DAT_PE = seg[4 * a + row]; //印出數值為[4*a + row]
    }

    return 0;
}

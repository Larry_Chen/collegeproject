#define GPIO_ACT_PC *((unsigned int *)0x001F6900)  //宣告GPIO_ACT_PC
#define GPIO_OEN_PC *((unsigned int *)0x001F6904)  //宣告GPIO_OEN_PC
#define GPIO_OMOD_PC *((unsigned int *)0x001F6908) //宣告GPIO_OMOD_PC
#define PAD_PC *((unsigned int *)0x001F690c)       //宣告PAD_PC
#define GPIO_DAT_PC *((unsigned int *)0x001F6910)  //宣告GPIO_DAT_PC
#define GPIO_REN_PC *((unsigned int *)0x001F6914)  //宣告GPIO_REN_PC
#define GPIO_RS_PC *((unsigned int *)0x001F6918)   //宣告GPIO_RS_PC
#define GPIO_ACT_PD *((unsigned int *)0x001F6980)  //宣告GPIO_ACT_PD
#define GPIO_OEN_PD *((unsigned int *)0x001F6984)  //宣告GPIO_OEN_PD
#define GPIO_OMOD_PD *((unsigned int *)0x001F6988) //宣告GPIO_OMOD_PD
#define PAD_PD *((unsigned int *)0x001F698C)       //宣告PAD_PD
#define GPIO_DAT_PD *((unsigned int *)0x001F6990)  //宣告GPIO_DAT_PD
#define GPIO_REN_PD *((unsigned int *)0x001F6994)  //宣告GPIO_REN_PD
#define GPIO_RS_PD *((unsigned int *)0x001F6998)   //宣告GPIO_RS_PD

#define RS 0x0080       //定義RS位址
#define E 0x0040        //定義E位址
#define RW 0x0020       //定義RW位址
#define CleanSet 0x0000 //定義CleanSet位址

void delay1(unsigned int nCount) //副程式 delay描述
{
    unsigned int i;
    for (i = 0; i < nCount; i++)
        ;
}

void CheckBusy(void) //BF ,CHACK BUSY FLAG
{
    unsigned short int i = 0x8000;
    while (i & 0x8000)
    {
        GPIO_ACT_PD = 0xFFFF; //Initialize GPIO_D outpot
        GPIO_OMOD_PD = 0x0;
        GPIO_OEN_PD = 0x0;

        GPIO_DAT_PD = (RW + E);

        GPIO_ACT_PD = 0xFF00; //Initialize GPIO_D input
        GPIO_RS_PD = 0xFF00;
        GPIO_REN_PD = 0xFF00;

        i = PAD_PD;

        GPIO_DAT_PD = CleanSet;
        delay1(100000);
    }
}

void WriteData(unsigned short int i)
{
    GPIO_ACT_PD = 0xFFFF; //Initialize GPIO_D outpot
    GPIO_OMOD_PD = 0x0;
    GPIO_OEN_PD = 0x0;

    GPIO_DAT_PD = ((i << 8) + RS + E);
    GPIO_DAT_PD = CleanSet;
    CheckBusy();
}

void WriteIns(unsigned short int instruction)
{
    GPIO_ACT_PD = 0xFFFF; //Initialize GPIO_D outpot
    GPIO_OMOD_PD = 0x0;
    GPIO_OEN_PD = 0x0;

    GPIO_DAT_PD = (instruction + E); // 指令?入操作：?入 RS=低、RW=低、EP=上升沿； ?出：?；
    GPIO_DAT_PD = CleanSet;
    CheckBusy();
}

void InitialLCD(void)
{
    WriteIns(0x3800); //FUNCTION SET
    WriteIns(0x3800);
    WriteIns(0x3800);
    WriteIns(0x3800);
    WriteIns(0x0800); // off display
    WriteIns(0x0100); // clear buffer
    WriteIns(0x0e00); // on display
    WriteIns(0x0600); // set input mode
}
void LCD_STR(char *str)
{

    WriteIns(0x3800); //FUNCTION SET
    WriteIns(0x0C00); //DISPLAY CONTROL
    WriteIns(0x0600); //SET INPUT MODE
    while (*str != 0)
        WriteData(*str++);
    WriteIns(0x0F00);
}
void SET_POS(unsigned char x, unsigned char y)
{
    unsigned int cmd;
    if (x < 1)
        x = 1;
    if (y < 1)
        y = 1;
    cmd = (x - 1) * 0x40 + (y - 1) + 0x80;
    WriteIns(0x3800); //FUNCTION SET
    WriteIns((cmd << 8));
    WriteIns(0x0600); //SET INPUT MODE
}
void Display_1Line(unsigned int WordValue)
{
    char ABC[] = {'A', 'b', 'C', 'd', 'E', 'f', 'G', 'h'};
    char i = 0;
    WriteIns(0x3800); //FUNCTION SET
    WriteIns(0x0C00); //DISPLAY CONTROL
    WriteIns(0x0600); //SET INPUT MODE
    for (i = 0; i < WordValue; i++)
        WriteData(ABC[i]);
}

void Display_2Line(int WordValue)
{
    char ABCD[] = {'10244'};
    char i;
    WriteIns(0x3800); //FUNCTION SET
    WriteIns(0x0C00); //DISPLAY CONTROL
    WriteIns(0x0600); //SET INPUT MODE

    WriteIns(0x8000); //1-LINE DD RAM SET Address
    for (i = 0; i < WordValue; i++)
        WriteData(ABCD[i]);

    WriteIns(0xC000); //2-LINE DD RAM SET Address
    for (i = 0; i < WordValue; i++)
        WriteData(ABCD[i]);
}

int main()
{

    int a = 5, b = 5;  //定義a和b =5
    int x, button = 0; //定義x和button=0
    InitialLCD();      //初始化LCD值和位址
    SET_POS(1, a);     //1為第一行、a為LCM上第幾個bit開始顯示
    LCD_STR("1024");   //設定其值是1024

    SET_POS(2, b);   //2為第二行、b為LCM上第幾個bit開始顯示
    LCD_STR("1042"); //設定其值是1042
    while (1)
    {
        x = PAD_PC;           //把按鍵讀進x
        button = x / 256;     //右移8bit
        button = button % 16; //取餘數
        while (button == 11)
        {
            delay1(10000);    //用delay來方便檢測
            if (button == 11) //若button的值為11
            {
                b = b + 1;       //第二行的值的位址右移1
                InitialLCD();    //讓LCD初始化
                SET_POS(1, a);   //1為第一行、a為LCM上第幾個bit開始顯示
                LCD_STR("1024"); //設定其值是1024
                SET_POS(2, b);   //2為第二行、b為LCM上第幾個bit開始顯示
                LCD_STR("1042"); //設定其值是1042
            }
            while (button == 11)
            {
                x = PAD_PC;           //把按鍵讀進x
                button = x / 256;     //右移8bit
                button = button % 16; //取餘數
            }
        }

        while (button == 7)
        {
            delay1(10000);   //用delay來方便檢測
            if (button == 7) //若button的值為7
            {
                b = b - 1;       //第二行的值的位址左移1
                InitialLCD();    //讓LCD初始化
                SET_POS(1, a);   //1為第一行、a為LCM上第幾個bit開始顯示
                LCD_STR("1024"); //設定其值是1024
                SET_POS(2, b);   //2為第二行、b為LCM上第幾個bit開始顯示
                LCD_STR("1042"); //設定其值是1042
            }
            while (button == 7)
            {
                x = PAD_PC;           //把按鍵讀進x
                button = x / 256;     //右移8bit
                button = button % 16; //取餘數
            }
        }

        while (button == 13)
        {
            delay1(10000);    //用delay來方便檢測
            if (button == 13) //若button的值為13
            {
                a = a - 1;       //第一行位址左移1
                InitialLCD();    //讓LCD初始化
                SET_POS(1, a);   //1為第一行、a為LCM上第幾個bit開始顯示
                LCD_STR("1024"); //設定其值是1024
                SET_POS(2, b);   //2為第二行、b為LCM上第幾個bit開始顯示
                LCD_STR("1042"); //設定其值是1042
            }
            while (button == 13)
            {
                x = PAD_PC;           //把按鍵讀進x
                button = x / 256;     //右移8bit
                button = button % 16; //取餘數
            }
        }

        while (button == 14)
        {
            delay1(10000);    //用delay來方便檢測
            if (button == 14) //若button的值為14
            {
                a = a + 1;       //第一行位址右移1
                InitialLCD();    //讓LCD初始化
                SET_POS(1, a);   //1為第一行、a為LCM上第幾個bit開始顯示
                LCD_STR("1024"); //設定其值是1024
                SET_POS(2, b);   //2為第二行、b為LCM上第幾個bit開始顯示
                LCD_STR("1042"); //設定其值是1042
            }
            while (button == 14)
            {
                x = PAD_PC;           //把按鍵讀進x
                button = x / 256;     //右移8bit
                button = button % 16; //取餘數
            }
        }
        delay1(10000); //用delay來方便檢測
    }
    return 0;
}
